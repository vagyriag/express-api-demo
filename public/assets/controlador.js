var controlador = (vista, modelo) => {

  vista.onSubirFoto = (email, foto, texto) => {
    var params = new FormData();
    params.set('foto', foto);
    params.set('texto', texto);

    fetch(`${location.origin}/api/subirFoto/${email}`, {
      method: 'POST',
      body: params
    })
    .then((res) => res.json())
    .then((res) => {
      console.log(res);
      vista.render();
    });
  };

  vista.onRegistro = (email, pass) => {
    var params = new URLSearchParams();
    params.set('email', email);
    params.set('pass', pass);

    fetch(`${location.origin}/api/crearUsuario`, {
      method: 'POST',
      body: params
    })
    .then((res) => res.json())
    .then((res) => console.log(res));

    vista.render('login');
  };

  vista.onLogin = (email, pass) => {
    var params = new URLSearchParams();
    params.set('email', email);
    params.set('pass', pass);

    fetch(`${location.origin}/api/login`, {
      method: 'POST',
      body: params
    })
    .then((res) => res.json())
    .then((res) => {
      if(res.mensaje == 'ok'){
        console.log(res);
        vista.usuario = res.usuario;
        vista.render('perfil');
      }
    });
  };

  // render inicial
  vista.render();


  fetch(`${location.origin}/api/usuarios`)
    .then((res) => res.json())
    .then((res) => {
      if(res.mensaje == 'ok'){
        vista.usuarios = res.usuarios;
        vista.render();
      }
    });
}

controlador(vista);