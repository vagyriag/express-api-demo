var vista = {
  usuario: null,
  usuarios: null,

  getHome(){
    var div = document.createElement('div');

    div.innerHTML = ``;

    if(!this.usuarios){
      div.innerHTML += `
        <h2>cargando...</h2>
      `;
    } else {
      div.innerHTML += '<button id="toLogin">login</button>';
      this.usuarios.sort((a, b) => a.email < b.email).forEach(user => {
        div.innerHTML += `
          <h2>
            <img src="fotos/${user.foto}" height="30" />
            ${user.email}
          </h2>
        `;
      });

      div.querySelector('#toLogin').addEventListener('click', () => {
        this.render('login');
      });
    }
    return div;
  },

  getPerfil(){
    var div = document.createElement('div');

    div.innerHTML = `
      <h1>${this.usuario.email}</h1>
    `;

    if(this.usuario.foto){
      div.innerHTML += `
        <img src="fotos/${this.usuario.foto}" />
        <p>${this.usuario.texto}</p>
      `;
    } else {
      div.innerHTML += `
        <form>
          <input type="file" name="foto" />
          <textarea name="texto"></textarea>
          <button type="submit">subir</submit>
        </form>
      `;

      div.querySelector('form').addEventListener('submit', (e) => {
        e.preventDefault();
        this.onSubirFoto(this.usuario.email, e.target.foto.files[0], e.target.texto.value);
      });
    }

    return div;
  },

  getRegistro() {
    var div = document.createElement('div');
    div.innerHTML = `
      <h2>Registro</h2>
      <form>
        <input name="email" placeholder="email" />
        <input name="pass" type="password" placeholder="contraseña" />
        <button type="submit">crear</submit>
      </form>
    `;

    div.querySelector('form').addEventListener('submit', (e) => {
      e.preventDefault();
      this.onRegistro(e.target.email.value, e.target.pass.value);
    });

    return div;
  },

  getLogin() {
    var div = document.createElement('div');
    div.innerHTML = `
      <h2>Login</h2>
      <form>
        <input name="email" placeholder="email" />
        <input name="pass" type="password" placeholder="contraseña" />
        <button type="submit">iniciar sesión</submit>
      </form>

      <button id="toRegistro">ir a registro</button>
    `;

    div.querySelector('form').addEventListener('submit', (e) => {
      e.preventDefault();
      this.onLogin(e.target.email.value, e.target.pass.value);
    });

    div.querySelector('#toRegistro').addEventListener('click', () => {
      this.render('registro');
    });

    return div;
  },

  render(pagina) {
    var main = document.getElementById('main');
    main.innerHTML = '';

    switch(pagina){
      default:
        main.appendChild(this.getHome());
        break;
      case 'registro':
        main.appendChild(this.getRegistro());
        break;
      case 'login':
        main.appendChild(this.getLogin());
        break;
      case 'perfil':
        main.appendChild(this.getPerfil());
        break;
    }
  }
}