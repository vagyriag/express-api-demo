const express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
const client = require('mongodb').MongoClient;
const fileUpload = require('express-fileupload');

var app = express();

app.use(fileUpload());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var url = 'mongodb://localhost:27017/ejemplo';
var db;

client.connect(url, (err, database) => {
  if(!err){
    db = database;
  }
});



app.get('/api/', (req, res) => {
  res.json({ message: 'working' });
});


app.get('/api/usuarios', (req, res) => {
  db.collection('usuarios')
    .find({})
    .toArray((err, usuarios) => {
      if(!err){
        res.json({ 
          mensaje: 'ok', 
          usuarios: usuarios,
        });
      }else{
        res.json({ mensaje: 'error' });
      }
    });
});


app.post('/api/crearUsuario', (req, res) => {
  db.collection('usuarios')
    .find({ email: req.body.email })
    .toArray((err, usuarios) => {
      if(!err && usuarios.length == 0){
        var nuevoUsuario = { 
          email: req.body.email,
          pass: req.body.pass
        };
        db.collection('usuarios').insert(nuevoUsuario, (errInsert) => {
          if(!errInsert){
            res.json({ mensaje: 'ok' });
          }else{
            res.json({ mensaje: 'no se pudo insertar usuario' });
          }
        });
      } else {
        res.json({ mensaje: 'no se pudo insertar usuario' });
      }
    });
});

app.post('/api/login', (req, res) => {
  db.collection('usuarios')
    .find({ email: req.body.email, pass: req.body.pass })
    .toArray((err, usuarios) => {
      if(!err && usuarios.length > 0){
        res.json({ 
          mensaje: 'ok',
          usuario: usuarios[0]
        });
      } else {
        res.json({ mensaje: 'usuario o contraseña incorrecto' });
      }
    });
});


app.post('/api/subirFoto/:email', (req, res) => {
  if (!req.files){
    return res.json({ mensaje: 'sin archivo' });
  }
 
  console.log(req.params.email, req.body.texto);
  var foto = req.files.foto;
 
  foto.mv(path.join(__dirname, `public/fotos/${foto.name}`), function(err) {
    if(!err){
      res.json({ mensaje: 'ok' });

      db.collection('usuarios')
        .updateOne({ email: req.params.email }, { 
          $set: {
            foto: foto.name,
            texto: req.body.texto
          }
        });

    }else{
      res.json({ mensaje: 'error', error: err });
    }
  });
});


// assets
app.use('/assets', express.static('public/assets'));
app.use('/fotos', express.static('public/fotos'));

// html
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});

const port = 3000;
app.listen(port, () => {
  console.log(`Listening on port ${port}`)
});