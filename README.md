## Instalación:
* Click derecho -> Git Bash Here
* git clone https://vagyriag@bitbucket.org/vagyriag/express-api-demo.git
* cd express-api-demo
* npm install

## Uso:
* node index.js
* Abrir [http://localhost:3000](http://localhost:3000)

## Nodemon:
* npm install -g nodemon
* nodemon index.js
